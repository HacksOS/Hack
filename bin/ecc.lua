if #({...}) < 1 then
	printError("Usage: ecc <curve>");
	return;
end

local Crypto = __load__ "/disk/modules/crypto/index.lua";

local curve = ({...})[1];

local sk = os.epoch("local");
local k = Crypto.createKeys(Crypto.ECC, curve);
local ek = os.epoch("local");

print("CURVE:", curve);
print("PRIVATE:", k.private);
print("X:", k.x);
print("Y:", k.y);

print("Took", ek - sk, "ms");