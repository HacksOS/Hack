local function recurseGetFiles (dir, files)
	dir = dir and dir or "/";
	files = files and files or { };
	
	for k, v in pairs(fs.list(dir)) do
		if fs.isDir(dir .. "/" .. v) then
			if v == ".git" then (function () end)();
			else recurseGetFiles(dir .. "/" .. v, files); end
		else
			files[#files + 1] = dir .. "/" .. v;
		end
	end
	
	return files;
end

local function getLines (file)
	local i = 0;
	local h = fs.open(file, "r");
	while h.readLine() ~= nil do i = i + 1; end
	h.close();
	return i;
end

local files = recurseGetFiles("/disk");
local lines = 0;

for index, fileName in pairs(files) do
	print(fileName);
	lines = lines + getLines(fileName);
end

print(#files, lines);