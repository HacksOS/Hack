local Crypto = __load__ "/disk/modules/crypto/index.lua";


local e = Crypto.encrypt("aes-256-cbc", "Hello", "world")
local d = Crypto.decrypt("aes-256-cbc", e, "world");

print(e)
print(d)