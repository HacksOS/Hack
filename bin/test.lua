print(shell.getRunningProgram())

local x, y = term.getCursorPos();
local obg, ofg = term.getBackgroundColor(), term.getTextColor();

local ch = string.char(157);

local fields = {
	{ "root", colors.purple },
	{ "/home", colors.blue },
	{ "$ master", colors.red },
};

for i = 1, #fields do
	local field = fields[i];
	local text = field[1];
	local color = field[2];
	term.setTextColor(colors.white);
	term.setBackgroundColor(color);
	term.write(" " .. text .. " ");
	term.setTextColor(color);
	term.setBackgroundColor(fields[i + 1] and fields[i + 1][2] or obg);
	term.write(ch);
end

print();
