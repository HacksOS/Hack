local Crypto = __load__ "/disk/modules/crypto/index.lua";
local keypair = __load__ "/disk/modules/crypto/eccincc.lua";

local tests = { };
local curves = {
	"secp112r1",
	"secp112r2",
	"secp128r1",
	"secp128r2",
	"secp160k1",
	"secp160r1",
	"secp160r2",
	"secp192k1",
	"secp192r1",
	"secp224k1",
	"secp224r1",
	"secp256k1",
	"secp256r1",
	"secp384r1",
	"secp521r1",
};

local function avg (args)
	local p = 0;
	for i = 1, #args do p = p + args[i]; end
	return p / #args;
end

local function performTest (curve)
	os.sleep(0);
	local s = os.epoch("local");
	Crypto.createKeys(Crypto.ECC, curve);
	local e = os.epoch("local");
	return e - s;
end

local function eccinccTest ()
	os.sleep(0);
	local s = os.epoch("local");
	keypair();
	local e = os.epoch("local");
	return e - s;
end

local N = 20;
print("Testing", N, "times");
local x, y = term.getCursorPos();
for i = 1, #curves do
	local curveTests = { };
	for a = 1, N do
		term.clearLine();
		term.setCursorPos(1, y);
		term.write(curves[i] .. " " .. tostring(i) .. "/" .. tostring(#curves+1) .. " " .. tostring(a) .. "/" .. tostring(N));
		curveTests[a] = performTest(curves[i]);
	end
	tests[i] = { curves[i], avg(curveTests) };
end
local curveTests = { };
for a = 1, N do
	term.clearLine();
	term.setCursorPos(1, y);
	term.write("ecc-in-cc " .. tostring(#curves+1) .. "/" .. tostring(#curves+1) .. " " .. tostring(a) .. "/" .. tostring(N));
	curveTests[a] = eccinccTest();
end
tests[#tests + 1] = { "ecc-in-cc", avg(curveTests) };
term.clearLine();
term.setCursorPos(1, y);

local ld = 0;
local l = 0;

for i = 1, #tests do
	local curve = tests[i][1];
	local ms = tests[i][2];
	local s = tostring(ms);
	local a = tostring(math.floor(ms));
	if #s > l then l = #s; end
	if #a > ld then ld = #a; end
end

local function padMs (ms, ld, l)
	local a = tostring(math.floor(ms));
	local s = tostring(ms);
	return (" "):rep(ld - #a) .. tostring(ms);
end

for i = 1, #tests do
	local curve = tests[i][1];
	local ms = tests[i][2];
	print("Curve", curve, "avg", padMs(math.floor(ms), ld, l), "ms");
end