local Crypto = __load__ "/disk/modules/crypto/index.lua";

local d, acb = Crypto.DiffieHellman.Alice();
local bs, bobB = Crypto.DiffieHellman.Bob(d[1], d[2], d[3]);
local as = acb(bobB);

print(bs, as, bs == as);