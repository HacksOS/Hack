local Crypto = __load__ "/disk/modules/crypto/index.lua";

local Alice = Crypto.DiffieHellman.Alice;
local Bob = Crypto.DiffieHellman.Bob;


local function generateSharedSecret (debug)
	if debug == true then print("Alice is generating domain parameters and public key"); end
	local aliceTime1, aliceDomain, aliceCB = Crypto.Test(Alice);
	if debug == true then print("Took", aliceTime1.final, "ms"); end

	if debug == true then print("Bob generates a public key and computes the secret"); end
	local bobTime, bobSecret, bobDomain = Crypto.Test(Bob, table.unpack(aliceDomain));
	if debug == true then print("Took", bobTime.final, "ms"); end

	if debug == true then print("Alice computes the secret"); end
	local aliceTime2, aliceSecret = Crypto.Test(aliceCB, bobDomain);
	if debug == true then print("Took", aliceTime2.final, "ms"); end

	if debug == true then print("Secrets", aliceSecret == bobSecret and "match" or "doesn't match"); end

	if debug == true then print("The entire process took", aliceTime2.stop - aliceTime1.start, "ms"); end
	
	return {
		aliceTime1 = aliceTime1, bobTime = bobTime, aliceTime2 = aliceTime2, final = {
			start = aliceTime1.start,
			stop = aliceTime2.stop,
			final = aliceTime2.stop - aliceTime1.start
		}
	}, aliceSecret;
end

local function avg (a)
	local l = #a;
	local v = 0;
	for i = 1, #a do v = v + a[i]; end
	return math.floor(v / l);
end

local function runTests (n)
	n = n and n or 100;
	local t = { };
	local x, y = term.getCursorPos();
	for i = 1, n do
		term.setCursorPos(1, y);
		term.clearLine();
		term.write(tostring(i) .. "/" .. tostring(n));
		local test = generateSharedSecret();
		t[i] = test.final.final;
		os.sleep(0);
	end
	return avg(t);
end

-- Script

local tests = 100;
local debug = false;


print("Running", tests, "tests");
local timeUsed = runTests(tests);
print("Took on avg", timeUsed, "ms");