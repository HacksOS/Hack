local Base = __load__ "/disk/modules/Base.lua";

Base.language "01";

local function pad (str, len, ch) return (ch):rep(len - #str); end
local function pads (str, len, ch) return pad(str, len, ch) .. str; end
local function pade (str, len, ch) return str .. pad(str, len, ch); end

local function randch ()
	local out = { };
	for i = 1, 16 do
		out[#out + 1] = string.char(math.random(0, 255));
		out[#out] = out[#out] .. string.char(math.random(0, 255));
	end
	return out;
end

local function getCharacter (...)
	local tbl = { ... };
	local s = "";
	for i = 1, #tbl do s = s .. tostring(tbl[i]); end
	local n = Base.decode(s);
	return string.char(n);
end

local function gen (map)
	local out = { };
	for i = 1, #map do
		out[i] = { };
		local line = map[i];
		for x = 1, #line do
			local ch = line:sub(x, x);
			local b = string.byte(ch);
			local bin = pads(Base.encode(b), 8, "0");
			for bx = 1, #bin do
				out[i][#out[i] + 1] = tonumber(bin:sub(bx, bx));
			end
		end
	end
	return out;
end

local map = gen {
	"ab", "cd", "ef", "gh",
	"ij", "kl", "mn", "op",
	"pq", "rs", "tu", "vw",
	"xy", "zæ", "øå", "AB"
};

map = gen(randch());

local function rotateX (y)
	map[y] = { map[y][#map[y]], table.unpack(map[y], 1, #map[y] - 1) };
	os.emit("toggle");
end

local function rotateY (x)
	local out = { map[#map][x] };
	for y = 1, #map[1] - 1 do
		out[#out + 1] = map[y][x];
	end
	for y = 1, #map do
		map[y][x] = out[y];
	end
	os.emit("toggle");
end

-- RENDERING --

local function renderMap ()
	local p = 1;
	local sx, sy = 2, 2;
	local bh = (p * 2) + 16;
	local tw, th = (p * 2) + 33, 1;
	
	paintutils.drawFilledBox(
		sx, sy,
		sx + tw - 1, sy + th - 1,
		colors.red
	);
	
	paintutils.drawFilledBox(
		sx, sy + th,
		sx + tw - 1, sy + th + bh - 1,
		colors.gray
	);
	
	term.setCursorPos(sx + p, sy);
	term.setBackgroundColor(colors.red);
	term.write("Binary Map");
	
end

local function renderView ()
	local p = 1;
	local sx, sy = 2, 22;
	local bh = (p * 2) + 17;
	local tw, th = (p * 2) + 33, 1;
	
	paintutils.drawFilledBox(
		sx, sy,
		sx + tw - 1, sy + th - 1,
		colors.red
	);
	
	paintutils.drawFilledBox(
		sx, sy + th,
		sx + tw - 1, sy + th + bh - 1,
		colors.gray
	);
	
	term.setCursorPos(sx + p, sy);
	term.setBackgroundColor(colors.red);
	term.write("Live View");
	
end

function renderBinaryData ()
	
	local p = 1;
	local sx, sy = 2, 2;
	local bh = (p * 2) + 17;
	local tw, th = (p * 2) + 33, 1;
	
	local bx = p + sx;
	local by = p + sy + th;
	
	term.setBackgroundColor(colors.gray);
	
	-- 31 down
	-- 16 right
	
	for y = 1, #map do
		term.setCursorPos(bx, by + y - 1);
		for x = 1, #map[y] do
			local c = tostring(map[y][x]);
			if c == "1" then term.setTextColor(colors.green);
			else term.setTextColor(colors.red); end
			term.write(c .. " ");
		end
		term.setTextColor(colors.white);
		term.write(string.char(16));
		if y == #map then
			term.setCursorPos(bx, by + y);
			term.setTextColor(colors.white);
			term.write((string.char(31) .. " "):rep(#map[y]));
		end
	end
end

local function renderCharacters ()
	local p = 1;
	local sx, sy = 2, 23;
	local bh = (p * 2) + 17;
	local tw, th = (p * 2) + 33, 1;
	
	local bx = p + sx;
	local by = p + sy + th;
	term.setBackgroundColor(colors.gray);
	for y = 1, #map do
		local xx = 0;
		for x = 1, #map[y], 8 do
			local bina = { table.unpack(map[y], x, x + 7) };
			local ch = getCharacter(table.unpack(bina));
			bina[#bina + 1] = " ";
			bina[#bina + 1] = ch;
			for i = 1, #bina do
				term.setCursorPos(bx + i + x - 2 + xx, by + y - 1);
				if bina[i] == 0 then term.setTextColor(colors.red);
				elseif bina[i] == 1 then term.setTextColor(colors.green);
				else term.setTextColor(colors.orange); end
				term.write(tostring(bina[i]));
			end
			xx = xx + 8;
		end
	end
end

local function render ()
	renderBinaryData();
	renderCharacters();
end

local function renderFunctions ()
	local p = 1;
	local sx, sy = 38, 2;
	local bh = (p * 2) + 5;
	local tw, th = (p * 2) + 9, 1;
	paintutils.drawFilledBox(
		sx, sy,
		sx + tw - 1, sy + th - 1,
		colors.red
	);
	
	paintutils.drawFilledBox(
		sx, sy + th,
		sx + tw - 1, sy + th + bh - 1,
		colors.gray
	);
	
	term.setCursorPos(sx + p, sy);
	term.setBackgroundColor(colors.red);
	term.setTextColor(colors.white)
	term.write("Functions");
	
	term.setBackgroundColor(colors.gray);
	
	term.setCursorPos(sx + p, sy + 2);
	term.write("Randomize");
	
	term.setCursorPos(sx + p, sy + 3);
	term.write("Rotate X");
	
	term.setCursorPos(sx + p, sy + 4);
	term.write("Rotate Y");
	
	term.setCursorPos(sx + p, sy + 5);
	term.write("Inverse");
	
	term.setCursorPos(sx + p, sy + 6);
	term.write("Empty");
	
end

term.clear();
renderFunctions();
renderMap();
renderBinaryData();
renderView();
renderCharacters();

local lastX = 0;
local lastY = 0;

local w, h = term.getSize();

local fam = {
	minX = 38,
	maxX = 48,
	[4] = function () -- Randomize
		map = gen(randch());
		render();
	end,
	[5] = function () -- Rotate X
		for i = 1, 16 do rotateX(i); end
		render();
	end,
	[6] = function () -- Rotate Y
		for i = 1, 16 do rotateY(i); end
		render();
	end,
	[7] = function () -- Inverse
		for y = 1, #map do
			for x = 1, #map[y] do
				map[y][x] = bit.bxor(map[y][x], 1);
			end
		end
		render();
	end,
	[8] = function () -- Empty
		for y = 1, #map do
			for x = 1, #map[y] do
				map[y][x] = 0;
			end
		end
		render();
	end
}

local wx, wy = 1, 1;

while true do
	local e, b, x, y = os.pullEvent();
	
	local action = false;
	
	local beginY, beginX = 4, 3;
	local pressedX, pressedY;
	if e == "mouse_click" or e == "mouse_drag" then
		action = true;
		pressedX = math.floor((x - beginX) / 2) + 1;
		pressedY = y - beginY + 1;
		if e ~= "mouse_click" and lastX == pressedX and lastY == pressedY then action = false; end
		term.setCursorPos(2, h - 1);
		term.setBackgroundColor(colors.black);
		term.setTextColor(colors.white);
		term.clearLine();
		print(e, x, y);
	end
	
	if e == "mouse_click" then
		if x >= fam.minX and x <= fam.maxX and fam[y] ~= nil then
			fam[y]();
			action = false;
		end
	end
	
	if action == true then
		if map[pressedY] ~= nil and map[pressedY][pressedX] ~= nil then
			map[pressedY][pressedX] = bit.bxor(map[pressedY][pressedX], 1);
			render();
		elseif map[pressedY] ~= nil and pressedX == #map[pressedY] + 1 then
			rotateX(pressedY);
			render();
		elseif map[pressedY] == nil and map[1][pressedX] ~= nil and pressedY == #map + 1 then
			rotateY(pressedX);
			render();
		end
		lastX, lastY = pressedX, pressedY;
	end
	
	if e == "char" then
		if wy == #map and wx > #map[wy] then
			wy, wx = 1, 1;
		elseif wx > #map[wy] then
			wx = 1;
			wy = wy + 1;
		end
		
		local binary = Base.encode(string.byte(b));
		for x = 1, #binary do
			local b = tonumber(binary:sub(x, x));
			map[wy][wx + x - 1] = b;
		end
		
		wx = wx + 8;
	end
end