if _G.initDone == true then return; end

if type(ccemux) == "table" then ccemux.attach("back", "wireless_modem", { range = 128, interdimensional = true, world = "main", posX = 0, posY = 0, posZ = 0 }); end

local function exit (message)
	if type(message) == "string" then
		printError(message);
	end
	shell.exit();
end

function _G.kind (of)
	return type(of) == "table" and getmetatable(of).kind or type(of);
end

local function getFileContent (path)
	if type(path) ~= "string" then error("Invalid argument #1 expected string (got " .. type(path) .. ")", 2); end
	if not fs.exists(path) then error("Invalid argument #1 path does not exist!", 2); end
	if fs.isDir(path) then error("Invalid argument #1 Cannot open directory!", 2); end
	local handle = fs.open(path, "r");
	local content = handle.readAll();
	handle.close();
	return content;
end

local function blendEnv (env2, env1)
	setmetatable(env1, { __index = env2 });
	return env1;
end

local function lazyLoad (path, env)
	if type(path) ~= "string" then error("Invalid argument #1 expected string (got " .. type(path) .. ")", 2); end
	if not fs.exists(path) then error("Invalid argument #1 path does not exist!", 2); end
	if fs.isDir(path) then error("Invalid argument #1 Cannot open directory!", 2); end
	env = type(env) == "table" and env or { };
	env = blendEnv(_G, env);
	local fileContent = getFileContent(path);
	local cb, err1 = load(fileContent, path);
	if err1 ~= nil then exit(err1) end
	setfenv(cb, env);
	local loaded;
	local ok, err2 = pcall(function () loaded = cb(); end);
	if not ok then exit(err2); end
	return loaded;
end

local Path = lazyLoad(fs.getDir(shell.getRunningProgram()) .. "/modules/Path.lua");

local function __loader__ (dir)
	if type(dir) ~= "string" then error("Invalid argument #1 expected string (got " .. type(dir) .. ")", 2); end
	if not fs.exists(dir) then error("Invalid argument #1 path does not exist!", 2); end
	if not fs.isDir(dir) then error("Invalid argument #1 Cannot create loader for file!", 2); end
	return function (path)
		local p = Path.resolve("/", dir, path);
		if type(p) ~= "string" then error("Invalid argument #1 expected string (got " .. type(p) .. ")", 2); end
		if not fs.exists(p) then error("Invalid argument #1 path does not exist!", 2); end
		if fs.isDir(p) then error("Invalid argument #1 Cannot open directory!", 2); end
		return lazyLoad(p, { __load__ = __loader__(Path.dirname(p)) });
	end
end

local _load_ = __loader__(fs.getDir(shell.getRunningProgram()));

local Threader = _load_ "./modules/Threader.lua";
local ThreadedEvents =  _load_ "./modules/ThreadedEvents.lua";

local threads = Threader();

os.threads = threads;
os.emitter = ThreadedEvents(threads);
os.on = os.emitter.on;
os.once = os.emitter.once;
os.off = os.emitter.off;
os.emit = os.emitter.emit;
os.createLoader = __loader__;

function _G.__load__ (path)
	-- term.write(Path.resolve("/", Path.dirname(shell.getRunningProgram())));
	local load = __loader__(Path.resolve("/", Path.dirname(shell.getRunningProgram())));
	return load(path);
end

_G.initDone = true;


-- Shell
shell.setPath(shell.path() .. ":/bin:/disk/bin");
local function clear ()
	term.redirect(term.native());
	term.setBackgroundColor(colors.black);
	term.setTextColor(colors.white);
	term.clear();
	term.setCursorPos(1, 1);
end
os.threads.create(function ()
	-- Shell Trap (trap the user inside a shell).
	while true do
		clear();
		shell.run("shell");
	end
end);
os.threads.start();
term.setBackgroundColor(colors.black);
term.setTextColor(colors.yellow);
term.setCursorBlink(false);
print("Bye!");
os.pullEvent = os.pullEventRaw;
sleep(3);
os.shutdown();
