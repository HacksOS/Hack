local Color = __load__ "./Color.lua";
local Base = __load__ "./Base.lua";

local Colors = {
	white     = Color(    1, 0x01, 0xF0F0F0),
	orange    = Color(    2, 0x02, 0xF2B233),
	magenta   = Color(    4, 0x04, 0xE57FD8),
	lightBlue = Color(    8, 0x08, 0x99B2F2),
	yellow    = Color(   16, 0x0F, 0xDEDE6C),
	lime      = Color(   32, 0x00, 0x7FCC19),
	pink      = Color(   64, 0x03, 0xF2B2CC),
	grey      = Color(  128, 0x05, 0x4C4C4C),
	lightGrey = Color(  256, 0x06, 0x999999),
	cyan      = Color(  512, 0x07, 0x4C99B2),
	purple    = Color( 1024, 0x09, 0xB266E5),
	blue      = Color( 2048, 0x0A, 0x3366CC),
	brown     = Color( 4096, 0x0B, 0x7F664C),
	green     = Color( 8192, 0x0C, 0x57A64E),
	red       = Color(16384, 0x0D, 0xCC4C4C),
	black     = Color(32768, 0x0E, 0x111111),
	inherit   = Color(65536, 0xFF, 0x000000)
};

Colors[    1] = Colors.white;
Colors[    2] = Colors.orange;
Colors[    4] = Colors.magenta;
Colors[    8] = Colors.lightBlue;
Colors[   16] = Colors.yellow;    Colors[15] = Colors.yellow;
Colors[   32] = Colors.lime;      Colors[ 0] = Colors.lime;
Colors[   64] = Colors.pink;      Colors[ 3] = Colors.pink;
Colors[  128] = Colors.grey;      Colors[ 5] = Colors.grey;
Colors[  256] = Colors.lightGrey; Colors[ 6] = Colors.lightGrey;
Colors[  512] = Colors.cyan;      Colors[ 7] = Colors.cyan;
Colors[ 1024] = Colors.purple;    Colors[ 9] = Colors.purple;
Colors[ 2048] = Colors.blue;      Colors[10] = Colors.blue;
Colors[ 4096] = Colors.brown;     Colors[11] = Colors.brown;
Colors[ 8192] = Colors.green;     Colors[12] = Colors.green;
Colors[16384] = Colors.red;       Colors[13] = Colors.red;
Colors[32748] = Colors.black;     Colors[14] = Colors.black;

function Colors.resolve (color)
	if color ~= nil then
		if
			type(color) ~= "table" and
			type(color) ~= "number" and
			type(color) ~= "string"
		then
			error("Invalid argument #1 expected table, number or string (got " .. type(color) .. ")", 2);
		end
	else
		error("Missing argument #1", 2);
	end
	if type(color) == "string" then
		if Colors[color] ~= nil then return Colors[color]; end
		if #color == 1 then color = tonumber(color, 16); end
		if color ~= nil then return Colors[color]; end
	elseif type(color) == "number" then
		return Colors[color];
	elseif type(color) == "table" and type(color.code) == "number" then
		return Colors[color.code];
	end
end

function Colors.combine (...)
	local args = { ... };
	for index, arg in pairs(args) do args[index] = Colors.resolve(arg); end
	local i = { };
	for index, arg in pairs(args) do
		if type(arg) == "table" then
			i[#i + 1] = arg.code;
		end
	end
	return bit.bor(table.unpack(i, 1, #i));
end

return Colors;
