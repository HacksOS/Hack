local function fnID (fn)
	if type(fn) ~= "function" then error("Invalid argument #1 expected function (got " .. type(fn) .. ")", 2); end
	local s = tostring(fn);
	return s:sub(11, #s);
end

local function ThreadedEvents (threads, extend)
	if type(threads) ~= "table" then error("Invalid argument #1 expected table (got " .. type(threads) .. ")", 2); end
	local self = type(extend) == "table" and extend or { };
	self._events = { };
	self._count = 0;
	
	function self.listeners (event)
		if event == nil then return self._count; end
		if type(event) ~= "string" then error("Invalid argument #1 expected string (got " .. type(event) .. ")", 2); end
		return (type(self._events[event]) == "table" and type(self._events[event]._count) == "number") and self._events[event]._count or 0;
	end
	
	function self.hasListeners (event)
		if event ~= nil and type(event) ~= "string" then error("Invalid argument #1 expected string (got " .. type(event) .. ")", 2); end
		return self.listeners(event) > 0;
	end
	
	function self.hasListener (event, listener)
		if type(event) ~= "string" then error("Invalid argument #1 expected string (got " .. type(event) .. ")", 2); end
		if type(listener) ~= "string" and type(listener) ~= "function" then error("Invalid argument #2 expected function (got " .. type(listener) .. ")", 2); end
		if type(listener) ~= "string" then listener = fnID(listener); end
		return self._events[event][listener] ~= nil;
	end
	
	function self.removeListener (event, listener)
		if event ~= nil and type(event) ~= "string" then error("Invalid argument #1 expected string (got " .. type(event) .. ")", 2); end
		if listener ~= nil and type(listener) ~= "function" and type(listener) ~= "string" then error("Invalid argument #2 expected string or function (got " .. type(listener) .. ")", 2); end
		if type(listener) == "function" then listener = fnID(listener); end
		if event ~= nil and listener ~= nil and self._events[event] ~= nil and self._events[event][listener] ~= nil then
			self._events[event][listener] = nil;
			self._events[event]._count = self._events[event]._count - 1;
			if self._events[event]._count < 1 then listner = nil; end
		end
		if event ~= nil and listener == nil and self._events[event] ~= nil then
			self._events[event] = nil;
			self._count = self._count - 1;
		end
		if event == nil then
			self._events = { };
			self._count = 0;
		end
	end
	
	function self.addListener (event, listener, once)
		if type(event) ~= "string" then error("Invalid argument #1 expected string (got " .. type(event) .. ")", 2); end
		if type(listener) ~= "function" then error("Invalid argument #2 expected function (got " .. type(listener) .. ")", 2); end
		local id = fnID(listener);
		if self._events[event] == nil then
			self._events[event] = { _count = 0 };
			self._count = self._count + 1;
		end
		if self._events[event][id] == nil then
			self._events[event][id] = { listener };
			if once == true then self._events[event][id][2] = true; end
			self._events[event]._count = self._events[event]._count + 1;
		end
		return self;
	end
	
	function self.on (event, listener)
		if type(event) ~= "string" then error("Invalid argument #1 expected string (got " .. type(event) .. ")", 2); end
		if type(listener) ~= "function" then error("Invalid argument #2 expected function (got " .. type(listener) .. ")", 2); end
		return self.addListener(event, listener, false);
	end
	
	function self.once (event, listener)
		if type(event) ~= "string" then error("Invalid argument #1 expected string (got " .. type(event) .. ")", 2); end
		if type(listener) ~= "function" then error("Invalid argument #2 expected function (got " .. type(listener) .. ")", 2); end
		return self.addListener(event, listener, true);
	end
	
	function self.off (event, listener)
		if event ~= nil and type(event) ~= "string" then error("Invalid argument #1 expected string (got " .. type(event) .. ")", 2); end
		if listener ~= nil and type(listener) ~= "function" and type(listener) ~= "string" then error("Invalid argument #2 expected string or function (got " .. type(listener) .. ")", 2); end
		return self.removeListener(event, listener, true);
	end
	
	function self.emit (event, ...)
		if type(event) ~= "string" then error("Invalid argument #1 expected string (got " .. type(event) .. ")", 2); end
		if not self.hasListeners(event) then return self; end
		local count = 0;
		local listeners = { };
		for id, list in pairs(self._events[event]) do
			if id ~= "_count" then
				local fn = list[1];
				local once = list[2] == true;
				if not once then listeners[id] = list; count = count + 1; end
				threads.create(fn, ...);
			end
		end
		if count > 0 then
			self._events[event] = listeners;
			self._events[event]._count = count;
		else
			self._events[event] = nil;
			self._count = self._count - 1;
		end
		return self;
	end
	
	return self;
end

return ThreadedEvents;