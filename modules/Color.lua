local function Color (code, icon, hex)
	local self = { };
	self.code = code;
	self.icon = icon;
	self.hex = hex;
	return self;
end

return Color;