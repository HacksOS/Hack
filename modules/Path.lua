
local Path = { };

local function trimWhitespace (str)
	if type(str) ~= "string" then error("Invalid argument #1 expected string (got " .. type(str) .. ")", 2); end
	local from = str:match("^%s*()");
	return from > #str and "" or str:match(".*%S", from);
end

local function trimSlashes (str)
	if type(str) ~= "string" then error("Invalid argument #1 expected string (got " .. type(str) .. ")", 2); end
	local len = #str;
	local abs = str:sub(1, 1) == "/";
	for i = len, 1, -1 do
		if str:sub(i, i) == "/" or str:sub(i, i) == "\\" then str = str:sub(1, i - 1);
		else return str; end
	end
	if abs and str:sub(1, 1) ~= "/" then str = "/" .. str; end
	return str;
end

local function trim (str)
	if type(str) ~= "string" then error("Invalid argument #1 expected string (got " .. type(str) .. ")", 2); end
	return trimSlashes(trimWhitespace(str));
end

function Path.split (str)
	if type(str) ~= "string" then error("Invalid argument #1 expected string (got " .. type(str) .. ")", 2); end
	str = trim(str);
	local parts = { "" };
	local len = #str;
	for i = 1, len do
		local part = str:sub(i, i);
		if part == "/" or part == "\\" then parts[#parts + 1] = "";
		else parts[#parts] = parts[#parts] .. part; end
	end
	return parts;
end

function Path.join ( ... )
	local parts = { ... };
	local value = "";
	for i = 1, #parts do
		if type(parts[i]) ~= "string" then error("Invalid argument #" .. tostring(i) .. " expected string (got " .. type(parts[i]) .. ")", 2); end
		parts[i] = trim(parts[i]);
		value = value .. parts[i] .. "/";
	end
	value = value:sub(1, #value - 1);
	return trim(value);
end

function Path.isAbsolute (str)
	if type(str) ~= "string" then error("Invalid argument #1 expected string (got " .. type(str) .. ")", 2); end
	str = trim(str);
	return str:sub(1, 1) == "/";
end

function Path.isRelative (str)
	if type(str) ~= "string" then error("Invalid argument #1 expected string (got " .. type(str) .. ")", 2); end
	str = trim(str);
	return not Path.isAbsolute(str);
end

function Path.dirname (str)
	if type(str) ~= "string" then error("Invalid argument #1 expected string (got " .. type(str) .. ")", 2); end
	str = trim(str);
	local parts = Path.split(str);
	if #parts == 0 then return "."; end
	if #parts == 1 then return "."; end
	if #parts == 2 and parts[1] == "" then return "/"; end
	parts[#parts] = nil;
	return Path.join(table.unpack(parts, 1, #parts));
end

function Path.filename (str)
	if type(str) ~= "string" then error("Invalid argument #1 expected string (got " .. type(str) .. ")", 2); end
	str = trim(str);
	local parts = Path.split(str);
	if #parts == 0 then return ""; end
	if #parts == 1 then return parts[1]; end
	if #parts == 2 and parts[2] == "" then return ""; end
	if #parts >= 2 then return parts[#parts]; end
	return parts[#parts];
end

function Path.extname (str)
	if type(str) ~= "string" then error("Invalid argument #1 expected string (got " .. type(str) .. ")", 2); end
	local filename = Path.filename(str);
	local ext = "";
	local ret = false;
	for i = 0, #filename do
		local part = filename:sub(#filename - i, #filename);
		local next = filename:sub(#filename - 1 - i, #filename);
		if part:sub(1, 1) == "." and #part > 1 and #next > #part and next:sub(1, 1) ~= "." then return part; end
	end
	return "";
end

function Path.basename (str)
	if type(str) ~= "string" then error("Invalid argument #1 expected string (got " .. type(str) .. ")", 2); end
	local filename = Path.filename(str);
	local extname = Path.extname(str);
	return filename:sub(1, #filename - #extname);
end

function Path.resolve ( ... )
	local parts = { };
	local unparts = { ... };
	local Actions = { };
	function Actions.back () if #parts > 0 then parts[#parts] = nil; end end
	function Actions.abs () parts = { }; end
	function Actions.default (part) parts[#parts + 1] = part; end
	for i = 1, #unparts do
		local bigPart = unparts[i];
		if type(bigPart) ~= "string" then error("Invalid argument #" .. tostring(i) .. " expected string (got " .. type(bigPart) .. ")"); end
		if (Path.isAbsolute(bigPart)) then Actions.abs(); end
		local smallParts = Path.split(bigPart);
		for x = 1, #smallParts do
			local part = trim(smallParts[x]);
			if part == "" then local _;
			elseif part ==  "." then local _;
			elseif part == ".." then Actions.back();
			else Actions.default(part); end
		end
	end
	if #parts == 0 then parts[1] = ""; end
	if #parts == 1 and parts[1] == "" then parts[2] = ""; end
	if #parts > 0 and parts[1] ~= "" then parts = { "", table.unpack(parts, 1, #parts) }; end
	return Path.join(table.unpack(parts, 1, #parts));
end

--
-- Export
--
if type(module) == "table" and type(module.exports) == "table" then module.exports = Path;
else return Path; end