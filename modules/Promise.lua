local Thread = __load__ "./Thread.lua";

local function Promise (threads, fn)
	local self = { };
	setmetatable(self, { kind = "Promise" });
	
	local thens = { };
	local catches = { };
	local resolved = false;
	local data = { };
	
	local function resolve (...)
		if resolved == true then return; end
		resolved = true;
		data = { ... };
		for index, cb in pairs(thens) do
			threads.create(cb, ...);
		end
		thens = { };
		catches = { };
	end
	
	local function reject (...)
		if resolved == true then return; end
		resolved = true;
		data = { ... };
		for index, cb in pairs(catches) do
			threads.create(cb, ...);
		end
		thens = { };
		catches = { };
	end
	
	local thread = Thread(function ()
		local result;
		local ok, err = pcall(function ()
			result = { fn(resolve, reject) };
		end);
		if not resolved then
			if not ok then reject(err); end
		end
	end);
	
	function self.done (cb)
		if type(cb) ~= "function" then error("Invalid argument #1 expected function (got " .. type(cb) .. ")", 2); end
		if resolved == true then Threads.create(cb, table.unpack(data)); return self; end
		thens[#thens + 1] = cb;
		return self;
	end
	
	function self.catch (cb)
		if type(cb) ~= "function" then error("Invalid argument #1 expected function (got " .. type(cb) .. ")", 2); end
		if resolved == true then Threads.create(cb, table.unpack(data)); return self; end
		catches[#catches + 1] = cb;
		return self;
	end
	
	threads.add(thread);
	
	return self;
end