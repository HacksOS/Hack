local function padString (str, len, char)
	local strlen = str:len();
	local out = "";
	if strlen < len then
		out = char:rep(len);
		out = out:sub(len - 1);
	end
	return out;
end

local function padStart (str, len, char)
	return padString(str, len, char) .. str;
end

local function encode (n)
	return padStart(string.format("%02x", n), 2, "0");
end

local function decode (hex)
	return tonumber("0x" .. hex);
end

return {
	encode = encode,
	decode = decode
};