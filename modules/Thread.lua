local function Thread (fn, ...)
	local self = { };
	self.forceKillState = false;
	self.data = { };
	self.fn = fn;
	self.args = { ... };
	self.thread = coroutine.create(function () fn(table.unpack(self.args)); end);
	self.host = nil;
	function self.running () return self.thread == coroutine.running(); end
	function self.dead () return self.forceKillState or coroutine.status(self.thread) == "dead"; end
	function self.sleeping () return not self.dead() and not self.running(); end
	function self.kill () self.forceKillState = true; if self.running() then coroutine.yield(); end end
	function self.resume (...) return coroutine.resume(self.thread, ...); end
	return self;
end

return Thread;