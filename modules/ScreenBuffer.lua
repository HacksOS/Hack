local Colors = __load__ "./Colors.lua";
local Events = __load__ "./Events.lua";

local function generateLine (width, background, foreground, character)
	local line = { };
	for x = 1, width do
		line[x] = { background, foreground, character };
	end
	return line;
end

local function generateMap (width, height, background, foreground, character)
	local map = { };
	for y = 1, height do
		map[y] = generateLine(width, background, foreground, character);
	end
	return map;
end

local function createBufferLine (buffer, y, width, background, foreground, character)
	for x = 1, width do
		buffer[#buffer + 1] = { x, y, background, foreground, character };
	end
end

local function createBufferLines (buffer, width, height, background, foreground, character)
	for y = 1, height do
		for x = 1, width do
			buffer[#buffer + 1] = { x, y, background, foreground, character };
		end
	end
end

local function border (max, value)
	return math.max(1, math.min(max, value));
end

local function getInherit (m, x, y)
	if type(x) == "number" and type(y) == "number" and type(m) == "table" and type(m[y]) == "table" and type(m[y][x]) == "table" then
		return m[y][x][1], m[y][x][2], m[y][x][3];
	end
end

local function ScreenBuffer (width, height)
	local self = { };
	local hook = Events();
	
	hook._map = generateMap(width, height, Colors.black, Colors.white, 0x00);
	hook._foreground = Colors.inherit;
	hook._background = Colors.inherit;
	hook._cursor = { 1, 1, 0 };
	hook._buffer = { };
	hook._self = self;
	hook._children = { };
	
	function hook.listen (buffer, x, y)
		hook._children[#hook._children + 1] = { buffer, x, y };
		buffer.on("child-render", function (child) hook.emit("child-render", child[1], child[2], child[3]); end);
		buffer.on("render", function () hook.emit("child-render", buffer, x, y); end);
		return hook;
	end
	
	function hook.width () return #hook._map[1]; end
	function hook.height () return #hook._map; end
	function hook.size () return hook.width(), hook.height(); end
	
	function hook.apply ()
		for i = 1, #hook._buffer do
			local item = hook._buffer[i];
			if type(hook._buffer[item[2]]) == "table" and type(hook._buffer[item[2]][item[1]]) == "table" then
				hook._map[item[2]][item[1]] = { item[3], item[4], item[5] };
			end
		end
		hook._buffer = { };
		hook.emit("render");
		return hook;
	end
	
	function hook.map (n, xx, yy)
		local map = { };
		xx, yy = 1, 1;
		for y = 1, #hook._map do
			map[y] = { };
			for x = 1, #hook._map[y] do
				local b = hook._map[y][x][1];
				local f = hook._map[y][x][2];
				local c = hook._map[y][x][3];
				local inb, inf, inc = getInherit(n, x + xx - 1, y + yy - 1);
				if c == 0 and b == Colors.inherit then
					b = inb and inb or b;
					f = inf and inf or f;
					c = inc and inc or c;
				else
					if b == Colors.inherit then b = inb and inb or b; end
					if f == Colors.inherit then f = inf and inf or f; end
				end
				map[y][x] = { b, f, c };
			end
		end
		local w, h = hook.size();
		for l = 1, #hook._children do
			local c = hook._children[l];
			local cx = c[1];
			local cy = c[2];
			local ch = c[3];
			local cm = ch.map(map, cx, cy);
			for y = 1, #cm do
				for x = 1, #cm[y] do
					if x < w + 1 and y < h + 1 then
						
					end
				end
			end
		end
		return map;
	end
	
	function hook.instructions (buffer, x, y)
		local ins = { };
		
		return ins;
	end
	
	function hook.x (x)
		if x == nil then return hook._cursor[1]; end
		if type(x) ~= "number" then error("Invalid type #1 expected number (got " .. type(x) .. ")", 2); end
		hook._cursor[1] = border(hook.width(), x);
		return hook;
	end
	
	function hook.y (y)
		if y == nil then return hook._cursor[2]; end
		if type(y) ~= "number" then error("Invalid type #1 expected number (got " .. type(y) .. ")", 2); end
		hook._cursor[1] = border(hook.height(), y);
		return hook;
	end
	
	function hook.cursor (x, y)
		if x == nil and y == nil then return hook.x(), hook.y(); end
		if x == nil and y ~= nil then
			if type(y) ~= "number" then error("Invalid type #2 expected number (got " .. type(y) .. ")", 2); end
			return hook.y(y);
		elseif x ~= nil and y == nil then
			if type(x) ~= "number" then error("Invalid type #1 expected number (got " .. type(x) .. ")", 2); end
			return hook.x(x);
		end
		if type(x) ~= "number" then error("Invalid type #1 expected number (got " .. type(x) .. ")", 2); end
		if type(y) ~= "number" then error("Invalid type #2 expected number (got " .. type(y) .. ")", 2); end
		return hook.x(x).y(y);
	end
	
	function hook.blink (b)
		if b == nil then return hook._cursor[3] == 1; end
		if type(b) ~= "boolean" then error("Invalid argument #1 expected boolean (got " .. type(b) .. ")", 2); end
		hook._cursor[3] = b and 1 or 0;
		return hook;
	end
	
	function hook.foreground (c)
		if c == nil then return hook._foreground; end
		if type(c) ~= "table" and type(c) ~= "number" and type(c) ~= "string" then error("Invalid argument #1 expected table, number or string (got " .. type(c) .. ")", 2); end
		local c = Colors.resolve(c);
		if c == nil then error("Invalid argument #1 invalid color", 2); end
		hook._foreground = c;
		return hook;
	end
	
	function hook.background (c)
		if c == nil then return hook._background; end
		if type(c) ~= "table" and type(c) ~= "number" and type(c) ~= "string" then error("Invalid argument #1 expected table, number or string (got " .. type(c) .. ")", 2); end
		local c = Colors.resolve(c);
		if c == nil then error("Invalid argument #1 invalid color", 2); end
		hook._background = c;
		return hook;
	end
	
	function hook.clear (line)
		if line == true then
			createBufferLine(hook._buffer, hook.y(), hook.width(), hook.background(), hook.foreground(), 0);
		else
			createBufferLines(hook._buffer, hook.width(), hook.height(), hook.background(), hook.foreground(), 0);
		end
		hook.emit("data");
		return hook;
	end
	
	function hook.write (...)
		local str = "";
		for index, arg in pairs({ ... }) do str = str .. " " .. tostring(arg); end
		local x, y = hook.pos();
		local width = hook.width();
		local foreground, background = hook.foreground(), hook.background();
		for i = 1, #str do
			local char = str:sub(i, i);
			hook._buffer[#hook._buffer + 1] = { border(width, x + i - 1), y, foreground, background, string.byte(str:sub(i, i)) };
			hook.emit("data");
		end
		hook._cursor[1] = border(hook.width(), x + #str);
		return hook;
	end
	
	function hook.blit (txt, fg, bg)
		if type(txt) ~= "string" then error("Invalid argument #1 expected string (got " .. type(txt) .. ")", 2); end
		if type(fg) ~= "string" then error("Invalid argument #2 expected string (got " .. type(fg) .. ")", 2); end
		if type(bg) ~= "string" then error("Invalid argument #3 expected string (got " .. type(bg) .. ")", 2); end
		if #txt == #fg then error("Invalid argument #2 does not match length with argument #1", 2); end
		if #txt == #bg then error("Invalid argument #3 does not match length with argument #1", 2); end
		for i = 1, #fg do
			if Colors.resolve(fg:sub(i, i)) == nil then
				error("Invalid argument #2 invalid character '" .. fg:sub(i, i) .. "', expected a color.", 2);
			end
		end
		for i = 1, #bg do
			if Colors.resolve(fg:sub(i, i)) == nil then
				error("Invalid argument #3 invalid character '" .. fg:sub(i, i) .. "', expected a color.", 2);
			end
		end
		local x, y = hook.pos();
		local width = hook.width();
		for i = 1, #txt do
			local char = string.byte(txt:sub(i, i));
			local f = Colors.resolve(fg:sub(i, i));
			local b = Colors.resolve(bg:sub(i, i));
			hook._buffer[#hook._buffer + 1] = { border(width, x + i - 1), y, f, b, char };
			hook.emit("data");
		end
		hook._cursor[1] = border(width, x + #txt);
		return hook;
	end
	
	function self.getSize () return hook.size(); end
	function self.getCursorPos () return hook.pos(); end
	function self.getCursorBlink () return hook.blink(); end
	function self.getTextColor () return hook.foreground(); end
	function self.getBackgroundColor () return hook.background(); end
	function self.clear () hook.clear(); end
	function self.clearLine () hook.clearLine(); end
	
	function self.setCursorPos (x, y)
		if type(x) ~= "number" then error("Invalid argument #1 expected number (got " .. type(x) .. ")", 2); end
		if type(y) ~= "number" then error("Invalid argument #2 expected number (got " .. type(y) .. ")", 2); end
		hook.pos(x, y);
	end
	
	function self.setCursorBlink (state)
		if type(state) ~= "boolean" then error("Invalid argument #1 expected boolean (got " .. type(state) .. ")", 2); end
		hook.blink(state);
	end
	
	function self.setTextColor (color)
		if type(color) ~= "number" then error("Invalid argument #1 expected number (got " .. type(color) .. ")", 2); end
		hook.foreground(color);
	end
	
	function self.setBackgroundColor (color)
		if type(color) ~= "number" then error("Invalid argument #1 expected number (got " .. type(color) .. ")", 2); end
		hook.background(color);
	end
	
	function self.write (text)
		hook.write(text).apply();
	end
	
	function self.blit (text, f, b)
		hook.blit(text, f, b).apply();
	end
	
	setmetatable(self, { hook = hook });
	return self;
end

local function getHook (screen)
	local meta = getmetatable(screen);
	return type(meta) == "table" and meta.hook or nil;
end

local function ScreenHook (...) return getHook(ScreenBuffer(...)); end
local function getBuffer (hook) return hook._self; end

return ScreenHook, ScreenBuffer, getHook, getBuffer;
