local function Modem (...)
	local self = { };
	
	local function _ (name, channel)
		if type(name) ~= "string" then error("Invalid argument #1 expected string (got " .. type(name) .. ")", 2); end
		if type(channel) ~= "number" then error("Invalid argument #1 expected number (got " .. type(channel) .. ")", 2); end
		if peripheral.getType(name) ~= "modem" then error("Invalid argument #1 peripheral is not a modem!", 2); end
		if channel < 1 or channel > 65535 then error("Invalid argument #2 out of range", 2); end
		self.name = name;
		self.modem = peripheral.wrap(name);
		self.channel = channel;
	end
	
	function self.receive (code, by)
		while true do
			local e, n, c, rc, m, d = os.pullEventRaw("modem_message");
			if n == self.name and type(m) == "table" and c == self.channel then
				if
					(type(m.target) ~= "number" or (type(m.target) == "number" and m.target == os.getComputerID())) and
					(type(code) ~= "string" or (type(code) == "string" and m.code == code)) and
					(type(by) ~= "number" or (type(by) == "number" and m.by == by))
				then
					return m.message;
				end
			end
		end
	end
	
	function self.send (message, target, code)
		if target ~= nil and type(target) ~= "number" then error("Invalid argument #2 expected number (got " .. type(target) .. ")", 2); end
		if code ~= nil and type(code) ~= "string" then error("Invalid argument #3 expected string (got " .. type(code) .. ")", 2); end
		local m = { };
		m.message = message;
		m.target = target,
		m.code = code;
		m.by = os.getComputerID();
		self.modem.transmit(self.channel, self.channel, m);
		return self;
	end
	
	_(...);
	return self;
end

return Modem;