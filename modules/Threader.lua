local Thread = __load__ "./Thread.lua";

local function threadID (thread)
	if type(thread) ~= "thread" then error("Invalid argument #1 expected thread (got " .. type(thread) .. ")", 2); end
	local s = tostring(thread);
	return s:sub(9, #s);
end

local function count (tbl)
	local i = 0;
	for k,v in pairs(tbl) do i = i + 1; end
	return i;
end

local function Threader ()
	local self = { };
	self.threads = { };
	self.forceBreak = false;
	
	function self.add (thread)
		if type(thread) ~= "table" then error("Invalid argument #1 expected thread (got " .. type(thread) .. ")", 2); end
		local id = threadID(thread.thread);
		if self.threads[id] ~= nil then error("Invalid argument #1 thread already exists!", 2); end
		self.threads[id] = thread;
		return self;
	end
	
	function self.remove (thread)
		if type(thread) ~= "thread" and type(thread) ~= "string" then error("Invalid argument #1 expected thread or string (got " .. type(thread) .. ")", 2); end
		if type(thread) == "thread" then thread = threadID(thread.thread); end
		self.threads[thread] = nil;
		return self;
	end
	
	function self.stop ()
		self.forceBreak = true
		return self;
	end
	
	function self.create (fn, ...)
		if type(fn) ~= "function" then error("Invalid argument #1 expected function (got " .. type(fn) .. ")", 2); end
		return self.add(Thread(fn, ...));
	end
	
	function self.start ()
		while true do
			if count(self.threads) < 1 then break; end
			if self.forceBreak == true then self.forceBreak = false; break; end
			local event_params = { os.pullEventRaw() };
			for id, thread in pairs(self.threads) do
				if self.forceBreak == true then break; end
				if not thread.dead() then
					if (type(thread.event) == "string" and event_params[1] == thread.event) or thread.event == nil then
						local result = { thread.resume(table.unpack(event_params)) };
						if self.forceBreak == true then break; end
						local ok, event = table.unpack(result);
						if not ok then printError(event); end
						if ok and type(event) == "string" then
							thread.event = event;
						else
							thread.event = nil;
						end
					end
				end
				if thread.dead() then self.remove(id); end
			end
		end
		return self;
	end
	
	return self;
end

return Threader;