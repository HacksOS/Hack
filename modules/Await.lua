local function Await (promise)
	local randomID = tostring({});
	promise
		.done(function (...) os.queueEvent(randomID .. "-done", ...); end)
		.catch(function (...) os.queueEvent(randomID .. "-fail", ...); end);
	while true do
		local results = { os.pullEvent() };
		if results[1] == randomID .. "-done" then
			return table.unpack(results, 2, #results);
		elseif results[1] == randomID .. "-fail" then
			error(results[2], 2);
		end
	end
end

return Await;