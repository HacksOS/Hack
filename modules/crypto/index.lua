local rsa = __load__ "./rsa/index.lua";
local ecc = __load__ "./ecc/index.lua";
local aes = __load__ "./aes/index.lua";

local Crypto = {
	-- Constants
	DiffieHellman = __load__ "./dh/index.lua",
	Test = __load__ "./Test.lua",
	ECC = 1,
	RSA = 2,
	
	-- (ECC) Curves
	curves = {
		secp112r1 = __load__ "./curves/secp112r1.lua",
		secp112r2 = __load__ "./curves/secp112r2.lua",
		secp128r1 = __load__ "./curves/secp128r1.lua",
		secp128r2 = __load__ "./curves/secp128r2.lua",
		secp160k1 = __load__ "./curves/secp160k1.lua",
		secp160r1 = __load__ "./curves/secp160r1.lua",
		secp160r2 = __load__ "./curves/secp160r2.lua",
		secp192k1 = __load__ "./curves/secp192k1.lua",
		secp192r1 = __load__ "./curves/secp192r1.lua",
		secp224k1 = __load__ "./curves/secp224k1.lua",
		secp224r1 = __load__ "./curves/secp224r1.lua",
		secp256k1 = __load__ "./curves/secp256k1.lua",
		secp256r1 = __load__ "./curves/secp256r1.lua",
		secp384r1 = __load__ "./curves/secp384r1.lua",
		secp521r1 = __load__ "./curves/secp521r1.lua"
	},
	
	-- Hashing Algorithms
	hashes = {
		sha256 = __load__ "./hashes/sha256.lua"
	},
	
	-- Ciphers
	ciphers = {
		["aes-128-ecb"] = aes(aes.ECB, aes.AES128),
		["aes-192-ecb"] = aes(aes.ECB, aes.AES192),
		["aes-256-ecb"] = aes(aes.ECB, aes.AES256),
		["aes-128-cbc"] = aes(aes.CBC, aes.AES128),
		["aes-192-cbc"] = aes(aes.CBC, aes.AES192),
		["aes-256-cbc"] = aes(aes.CBC, aes.AES256),
		["aes-128-cfb"] = aes(aes.CFB, aes.AES128),
		["aes-192-cfb"] = aes(aes.CFB, aes.AES192),
		["aes-256-cfb"] = aes(aes.CFB, aes.AES256),
		["aes-128-ctr"] = aes(aes.CTR, aes.AES128),
		["aes-192-ctr"] = aes(aes.CTR, aes.AES192),
		["aes-256-ctr"] = aes(aes.CTR, aes.AES256)
	}
};

function Crypto.getCurves ()
	local names = { };
	for key, curve in pairs(Crypto.curves) do
		names[#names + 1] = key;
	end
	return names;
end

function Crypto.getHashes ()
	local names = { };
	for key, curve in pairs(Crypto.hashes) do
		names[#names + 1] = key;
	end
	return names;
end

function Crypto.getCiphers ()
	local names = { };
	for key, curve in pairs(Crypto.ciphers) do
		names[#names + 1] = key;
	end
	return names;
end

function Crypto.createHash (hash, data)
	if type(hash) ~= "string" then error("Invalid argument #1 expected string (got " .. type(hash) .. ")", 2); end
	if type(Crypto.hashes[hash]) ~= "table" then error("Invalid argument #1 unknown hash", 2); end
	return Crypto.hashes.compute(data);
end

function Crypto.createHmac (hash, data, key)
	if type(hash) ~= "string" then error("Invalid argument #1 expected string (got " .. type(hash) .. ")", 2); end
	if type(Crypto.hashes[hash]) ~= "table" then error("Invalid argument #1 unknown hash", 2); end
	if type(Crypto.hashes[hash].hmac) ~= "function" then error("Invalid argument #1 hash doesn't support hmac", 2); end
	return Crypto.hashes.hmac(data, key);
end

function Crypto.createKeys (t, o)
	if type(t) ~= "number" then error("Invalid argument #1 expected number (got " .. type(t) .. ")", 2); end
	if t == Crypto.RSA then
		return rsa.generateKeys();
	elseif t == Crypto.ECC then
		if type(o) ~= "string" then error("Invalid argument #2 expected string (got " .. type(o) .. ")", 2); end
		if type(Crypto.curves[o]) ~= "table" then error("Invalid argument #2 invalid curve", 2); end
		return ecc.generateKeys(Crypto.curves[o]);
	else
		error("Invalid argument #1 invalid type");
	end
end

function Crypto.sign (t, o, k, m)
	if type(t) ~= "number" then error("Invalid argument #1 expected number (got " .. type(t) .. ")", 2); end
	if t == Crypto.RSA then
		error("Signing for RSA has not been implemented yet!", 2);
	elseif t == Crypto.ECC then
		if type(o) ~= "string" then error("Invalid argument #2 expected string (got " .. type(o) .. ")", 2); end
		if type(Crypto.curves[o]) ~= "table" then error("Invalid argument #2 invalid curve", 2); end
		if type(k) ~= "table" then error("Invalid argument #3 expected table (got " .. type(k) ..")", 2); end
		if type(m) ~= "string" then error("Invalid argument #4 expected string (got " .. type(m) .. ")", 2); end
		return ecc.sign(Crypto.curves[o], k, m);
	else
		error("Invalid argument #1 invalid type");
	end
end

function Crypto.verify (t, o, k, s)
	if type(t) ~= "number" then error("Invalid argument #1 expected number (got " .. type(t) .. ")", 2); end
	if t == Crypto.RSA then
		error("Signature verification for RSA has not been implemented yet!", 2);
	elseif t == Crypto.ECC then
		if type(o) ~= "string" then error("Invalid argument #2 expected string (got " .. type(o) .. ")", 2); end
		if type(Crypto.curves[o]) ~= "table" then error("Invalid argument #2 invalid curve", 2); end
		if type(k) ~= "table" then error("Invalid argument #3 expected table (got " .. type(k) ..")", 2); end
		if type(m) ~= "string" then error("Invalid argument #4 expected string (got " .. type(m) .. ")", 2); end
		return ecc.verify(Crypto.curves[o], k[1], k[2], s);
	else
		error("Invalid argument #1 invalid type");
	end
end

function Crypto.encrypt (cipher, text, key)
	if type(cipher) ~= "string" then error("Invalid argument #1 expected string (got " .. type(cipher) .. ")", 2); end
	if type(Crypto.ciphers[cipher]) ~= "table" then error("Invalid argument #1 unknown cipher", 2); end
	return Crypto.ciphers[cipher].encrypt(text, key);
end

function Crypto.decrypt (cipher, text, key)
	if type(cipher) ~= "string" then error("Invalid argument #1 expected string (got " .. type(cipher) .. ")", 2); end
	if type(Crypto.ciphers[cipher]) ~= "table" then error("Invalid argument #1 unknown cipher", 2); end
	return Crypto.ciphers[cipher].decrypt(text, key);
end

return Crypto;