local sha = __load__ "../hashes/sha256.lua";
local aes = __load__ "./aes.lua";
local Hex = __load__ "../../Hex.lua";


local function getByte () return math.random(0, 255); end

local function getBytes (n)
	local bytes = { };
	for i = 1, n do bytes[i] = getByte(); end
	return bytes;
end

local function bytesToHex (bytes)
	local s = "";
	for i = 1, #bytes do s = s .. Hex.encode(bytes[i]); end
	return s;
end

local function hexToBytes (hex)
	local bytes = { };
	for i = 1, #hex, 2 do
		bytes[i] = Hex.decode(hex:sub(i, i + 1));
	end
	return bytes;
end

local function bytesToString (bytes)
	local s = "";
	for i = 1, #bytes do s = s .. string.char(bytes[i]); end
	return s;
end

local function stringToBytes (str)
	local bytes = { };
	for i = 1, #str do bytes[i] = string.byte(str:sub(i, i)); end
	return bytes;
end

local function cipher (mode, size)
	local self = { };
	
	function self.encrypt (text, key)
		local k = sha.hmac(key, key);
		local iv1 = getBytes(16);
		local iv2 = getBytes(16);
		local e1 = aes.encrypt(k, text, size, mode, iv1);
		local e2 = aes.encrypt(k, bytesToString(iv1) .. e1, size, mode, iv2);
		local m = bytesToString(iv2) .. e2;
		return bytesToHex(stringToBytes(m));
	end
	
	function self.decrypt (text, key)
		local k = sha.hmac(key, key);
		text = bytesToString(hexToBytes(text));
		local iv2, dec0 = stringToBytes(text:sub(1, 16)), text:sub(17, #text);
		local vdec0;
		local pdec0 = pcall(function () vdec0 = aes.decrypt(k, dec0, size, mode, iv2); end);
		if not pdec0 then return false; end
		local iv1, dec1 = stringToBytes(vdec0:sub(1, 16)), vdec0:sub(17, #vdec0);
		local vdec1;
		local pdec1 = pcall(function () vdec1 = aes.decrypt(k, dec1, size, mode, iv1); end)
		if not pdec1 then return false;
		else return vdec1; end
	end
	
	return self;
end

return setmetatable({ }, { __call = function (self, ...) return cipher(...); end, __index = aes });