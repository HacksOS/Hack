local function Point (tbl)
	if type(tbl) ~= "table" then error("Invalid argument #1 expected table (got " .. type(tbl) .. ")", 2); end
	if #tbl < 2 or #tbl > 3 then error("Point must be either 2- or 3 dimensional!", 2); end
	local self = { };
	self.d = #tbl;
	self.x = tbl[1];
	self.y = tbl[2];
	self.z = tbl[3];
	return self;
end

return Point;