local Big = __load__ "../../Big.lua";

local function mod (a, b) return a - ((a / b) * b); end

local function ECPoint (curve, x, y)
	local self = { };
	
	self.x = x;
	self.y = y;
	
	function self.add (q)
		local s = mod((self.y - q.y) / (self.x - q.x), curve.P);
		local x = mod((s * s) - self.x - q.x, curve.P);
		local y = mod(-self.y + (s * (self.x - x)), curve.P);
		return ECPoint(curve, x, y);
	end
	
	function self.double (c)
		local Dx = c * self.x;
		local Dy = c * self.y;
		local s = mod((((c + 1) * self.x) ^ 2) / (Dy), curve.P);
		local x = mod((s * s) - Dx, curve.P);
		local y = mod(-Dy + (s * (Dx - x)), curve.P);
		return ECPoint(curve, x, y);
	end
	
	setmetatable(self, {
		__add = function (s, Q)
			return self.add(q);
		end,
		__mul = function (s, n)
			return self.double(n);
		end
	});
	return self;
end

return ECPoint;