local Big = __load__ "../../Big.lua";
local ECPoint = __load__ "./ECPoint.lua";
local Base = __load__ "../../Base.lua";

Base.language "0123456789ABCDEF";

local function createPrivateKey (curve)
	local c = tostring(curve.n);
	local rand = "";
	for i = 1, #c do
		max = tonumber(c:sub(i, i));
		rand = rand .. tostring(math.random(0, max));
	end
	local r = Big(rand);
	if Big.le(curve.n, r) then return createPrivateKey(curve); end
	return r;
end

local function generateKeys (curve)
	local G = ECPoint(curve, curve.G.x, curve.G.y);
	local d = createPrivateKey(curve);
	local Q = G * d;
	return {
		private = tostring(d),
		x = tostring(Q.x),
		y = tostring(Q.y)
	};
end

return {
	generateKeys = generateKeys,
	encrypt = encrypt,
	decrypt = decrypt
};
