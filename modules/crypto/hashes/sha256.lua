-- SHA-256, HMAC and PBKDF2 functions in ComputerCraft
-- By Anavrins
local bit     = _G.bit;
local mod32   = 2 ^ 32;
local band    = bit32 and bit32.band    or bit.band;
local bnot    = bit32 and bit32.bnot    or bit.bnot;
local bxor    = bit32 and bit32.bxor    or bit.bxor;
local blshift = bit32 and bit32.blshift or bit.blshift;
local upack   = unpack or table.unpack;

local function rrotate (n, b)
	local s = n / (2 ^ b);
	local f = s % 1;
	return (s - f) + f * mod32;
end

local function brshift (int, by)
	local s = int / (2 ^ by)
	return s - s % 1;
end

local H = { 0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
						0x510E527F, 0x9b05688C, 0x1f83d9AB, 0x5bE0CD19 };

local K = { 0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5,
						0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174,
						0xE49B69C1, 0xEFBE4786, 0x0FC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
						0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x06CA6351, 0x14292967,
						0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85,
						0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
						0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3,
						0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2 };

local function counter (inc)
	local t1, t2 = 0, 0;
	if 0xFFFFFFFF - t1 < inc then
		t2 = t2 + 1;
		t1 = inc - (0xFFFFFFFF - t1) - 1;
	else
		t1 = t1 + inc;
	end
	return t2, t1;
end

local function BE_toInt (bs, i)
	return blshift((bs[i] or 0), 24) + blshift((bs[i + 1] or 0), 16) + blshift((bs[i + 2] or 0), 8) + (bs[i + 3] or 0);
end

local function preprocess (data)
	local len = #data;
	local proc = { };
	data[#data + 1] = 0x80;
	while #data % 64 ~= 56 do
		data[#data + 1] = 0;
	end
	local blocks = math.ceil(#data / 64);
	for i = 1, blocks do
		proc[i] = { };
		for j = 1, 16 do
			proc[i][j] = BE_toInt(data, 1 + ((i - 1) * 64) + ((j - 1) * 4));
		end
	end
	proc[blocks][15], proc[blocks][16] = counter(len * 8);
	return proc;
end

local function digestblock (w, C)
	for j = 17, 64 do
		local s0 = bxor(bxor(rrotate(w[j - 15], 7), rrotate(w[j - 15], 18)), brshift(w[j - 15], 3));
		local s1 = bxor(bxor(rrotate(w[j - 2], 17), rrotate(w[j - 2], 19)), brshift(w[j - 2], 10));
		w[j] = (w[j - 16] + s0 + w[j - 7] + s1) % mod32;
	end
	local a, b, c, d, e, f, g, h = upack(C);
	for j = 1, 64 do
		local S1 = bxor(bxor(rrotate(e, 6), rrotate(e, 11)), rrotate(e, 25));
		local ch = bxor(band(e, f), band(bnot(e), g));
		local temp1 = (h + S1 + ch + K[j] + w[j]) % mod32;
		local S0 = bxor(bxor(rrotate(a, 2), rrotate(a, 13)), rrotate(a, 22));
		local maj = bxor(bxor(band(a, b), band(a, c)), band(b, c));
		local temp2 = (S0 + maj) % mod32;
		h, g, f, e, d, c, b, a = g, f, e, (d + temp1) % mod32, c, b, a, (temp1 + temp2) % mod32;
	end
	C[1] = (C[1] + a) % mod32;
	C[2] = (C[2] + b) % mod32;
	C[3] = (C[3] + c) % mod32;
	C[4] = (C[4] + d) % mod32;
	C[5] = (C[5] + e) % mod32;
	C[6] = (C[6] + f) % mod32;
	C[7] = (C[7] + g) % mod32;
	C[8] = (C[8] + h) % mod32;
	return C;
end

local mt = {
	__tostring = function (a) return string.char(upack(a)); end,
	__index = {
		toHex = function (self) return ("%02x"):rep(#self):format(upack(self)); end,
		isEqual = function (self, t)
			if type(t) ~= "table" or #self ~= #t then return false; end
			local ret = 0;
			for i = 1, #self do
				ret = bit32.bor(ret, bxor(self[i], t[i]));
			end
			return ret == 0;
		end
	}
};

local function toBytes (t, n)
	local b = { };
	for i = 1, n do
		b[(i - 1) * 4 + 1] = band(brshift(t[i], 24), 0xFF);
		b[(i - 1) * 4 + 2] = band(brshift(t[i], 16), 0xFF);
		b[(i - 1) * 4 + 3] = band(brshift(t[i], 8), 0xFF);
		b[(i - 1) * 4 + 4] = band(t[i], 0xFF);
	end
	return setmetatable(b, mt);
end

local function digest (data)
	data = data or "";
	data = type(data) == "table" and { upack(data) } or { tostring(data):byte(1, -1) };
	data = preprocess(data);
	local C = { upack(H) };
	for i = 1, #data do
		C = digestblock(data[i], C);
	end
	return toBytes(C, 8);
end

local function hmac (data, key)
	data = type(data) == "table" and { upack(data) } or { tostring(data):byte(1, -1) };
	key = type(key) == "table" and { upack(key) } or { tostring(key):byte(1, -1) };
	local blocksize = 64;
	key = #key > blocksize and digest(key) or key;
	local ipad = { };
	local opad = { };
	local padded_key = { };
	for i = 1, blocksize do
		ipad[i] = bxor(0x36, key[i] or 0);
		opad[i] = bxor(0x5C, key[i] or 0);
	end
	for i = 1, #data do
		ipad[blocksize + i] = data[i];
	end
	ipad = digest(ipad);
	for i = 1, blocksize do
		padded_key[i] = opad[i];
		padded_key[blocksize + i] = ipad[i];
	end
	return digest(padded_key):toHex();
end

local function compute (data)
	return digest(data):toHex();
end

return {
	digest = digest,
	compute = compute,
	hmac = hmac
};