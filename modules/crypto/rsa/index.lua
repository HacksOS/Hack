local Big = __load__ "../../Big.lua";
local Base = __load__ "../../Base.lua";
Base.language "0123456789ABCDEF";
local primes = { 4001, 4003, 4007, 4013, 4019, 4021, 4027, 4049, 4051, 4057, 4073, 4079, 4091, 4093, 4099, 4111, 4127, 4129, 4133, 4139, 4153, 4157, 4159, 4177, 4201, 4211, 4217, 4219, 4229, 4231, 4241, 4243, 4253, 4259, 4261, 4271, 4273, 4283, 4289, 4297, 4327, 4337, 4339, 4349, 4357, 4363, 4373, 4391, 4397, 4409, 4421, 4423, 4441, 4447, 4451, 4457, 4463, 4481, 4483, 4493, 4507, 4513, 4517, 4519, 4523, 4547, 4549, 4561, 4567, 4583, 4591, 4597, 4603, 4621, 4637, 4639, 4643, 4649, 4651, 4657, 4663, 4673, 4679, 4691, 4703, 4721, 4723, 4729, 4733, 4751, 4759, 4783, 4787, 4789, 4793, 4799, 4801, 4813, 4817, 4831, 4861, 4871, 4877, 4889, 4903, 4909, 4919, 4931, 4933, 4937, 4943, 4951, 4957, 4967, 4969, 4973, 4987, 4993, 4999, 5003 };
primes = { 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113 };
-- primes = { 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601 };
local function getRandomPrime () return primes[math.random(1, #primes)]; end
local function getBigPrime () return Big(getRandomPrime()); end
local function bigRandom () return Big(math.random(100, 1000)); end
local function mod (a, b) return a - ((a / b) * b); end
local function pow (a, b, p)
	local i = 0;
	local sleepAt = 100;
	local r = Big(0);
	while not Big.eq(b, Big(1)) do
		if i == sleepAt then i = 0; sleep(0); end
		i = i + 1;
		b = b - 1;
		if Big.eq(r, Big(0)) then
			r = mod(a * a, p);
		else
			r = mod(r * a, p);
		end
	end
	return r;
end

local function generateKeyPair ()
	local p, q, N, phi, e, d;
	local zero = Big(0);
	
	p = getBigPrime();
	q = getBigPrime();
	
	while Big.eq(p, q) do q = getBigPrime(); end
	
	N = p * q;
	phi = (p - 1) * (q - 1);
	e = Big(0);
	
	while Big.eq(e, zero) do
		local prime = getBigPrime();
		if
			not Big.eq(e, p) and
			not Big.eq(e, q) and
			mod(phi, prime) > Big(0)
		then
			e = prime;
		end
	end
	
	local i = Big(3);
	local stop = phi;
	
	while true do
		if Big.eq(i, stop) then break; end
		if Big.eq(mod((i * phi) + Big(1), e), zero) then
			d = ((i * phi) + 1) / e;
			break;
		end
		i = i + 1;
	end
	
	return {
		public = e,
		private = d,
		product = N
	};
end

local function switch (msg, key, pro)
	return pow(Big(msg), Big(key), Big(pro));
end

local function stringToBytes (message)
	if type(message) ~= "string" then error("Invalid argument #1 expected string (got " .. type(message) .. ")", 2); end
	local bytes = { };
	for i = 1, #message do
		bytes[i] = string.byte(message:sub(i, i)) + 1;
	end
	return bytes;
end

local function bytesToString (bytes)
	if type(bytes) ~= "table" then error("Invalid argument #1 expected table (got " .. type(bytes) .. ")", 2); end
	local str = "";
	for i = 1, #bytes do
		if type(bytes[i]) == "table" then bytes[i] = tonumber(tostring(bytes[i])); end
		if type(bytes[i]) ~= "number" then error("Invalid property [" .. tostring(i) .. "] in argument #1 expected number (got " .. type(bytes[i]) .. ")", 2); end
		str = str .. string.char(bytes[i] - 1);
	end
	return str;
end

local function bytesToHex (bytes)
	local longest = 0;
	for i = 1, #bytes do
		bytes[i] = Base.encode(tonumber(tostring(bytes[i])));
		if #bytes[i] > longest then longest = #bytes; end
	end
	local s = "";
	for i = 1, #bytes do
		s = s .. (i == 1 and "" or string.char(0)) .. ("0"):rep(math.max(0, longest - #bytes[i] - 1)) .. bytes[i];
	end
	return s;
end

local function hexToBytes (hex)
	local bytes = { };
	local b = "";
	for i = 1, #hex do
		if hex:sub(i, i) == string.char(0) then
			bytes[#bytes + 1] = Base.decode(b);
			b = "";
		else
			b = b .. hex:sub(i, i);
		end
	end
	if b ~= "" then
		bytes[#bytes + 1] = Base.decode(b);
	end
	return bytes;
end

local function compute (bytes, key, pro)
	for i = 1, #bytes do
		bytes[i] = switch(bytes[i], key, pro);
	end
	return bytes;
end

local function encrypt (msg, key, pro)
	return bytesToHex(compute(stringToBytes(msg), key, pro));
end

local function decrypt (msg, key, pro)
	return bytesToString(compute(hexToBytes(msg), key, pro));
end

return {
	generateKeyPair = generateKeyPair,
	switch = switch,
	compute = compute,
	encrypt = encrypt,
	decrypt = decrypt
};
