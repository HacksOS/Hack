local function Test (fn, ...)
	local _s = os.epoch("local");
	local _a = { fn(...) };
	local _e = os.epoch("local");
	
	return {
		final = _e - _s,
		stop = _e,
		start = _s
	}, table.unpack(_a);
end

return Test;