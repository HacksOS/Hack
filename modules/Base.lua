--
-- A Lua Base converter.
--

local Base = { };
local Private = { };

--
-- `Private.language` property is a string of characters that you want to use as
-- your base.
--
Private.language = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

--
-- `Private.language_inversed` is a table containing the characters as keys with
-- their position as their value.
--

--
-- `Private.language_length` is a number containing the amount of characters in
-- the language.
--

function Private.verifyDuplicates (data)
	local used = { };
	for i = 1, #data do
		local c = data:sub(i, i);
		if not used[c] then used[c] = true;
		else return false; end
	end
	return true;
end

function Private.turnIntoTable (data)
	local lang = { };
	local inv = { };
	local n = 0;
	for i = 1, #data do
		lang[n] = data:sub(i, i);
		inv[data:sub(i, i)] = n;
		n = n + 1;
	end
	return lang, inv, n;
end

function Private.verifyBase (str)
	for i = 1, #str do
		if Private.language_inversed[str:sub(i, i)] == nil then return false; end
	end
	return true;
end

Private.language, Private.language_inversed, Private.language_length = Private.turnIntoTable(Private.language);

--
-- Here you can either set or get the base language.
--
-- **Note**: You cannot reuse characters.
--
function Base.language (value)
	if type(value) ~= "string" then return Private.language; end
	if not Private.verifyDuplicates(value) then error("Invalid argument #1 characters cannot be reused!", 2); end
	Private.language, Private.language_inversed, Private.language_length = Private.turnIntoTable(value);
	return Base;
end

function Base.encode (n)
	if type(n) ~= "number" then error("Invalid argument #1 expected number (got " .. type(n) .. ")", 2); end
	local p = true; -- is number positive?
	n = math.floor(n);
	if n < 0 then p = false; n = n * -1; end
	if n == 0 then return Private.language[0]; end
	local s = "";
	while math.floor(n) > 0 do
		s = Private.language[ math.floor(n) % Private.language_length ] .. s;
		n = math.floor(n) / Private.language_length;
	end
	return p and s or "-" .. s;
end

function Base.decode (s)
	if type(s) ~= "string" then error("Invalid argument #1 expected string (got " .. type(s) .. ")", 2); end
	local p = true; -- is number positive?
	local n = 0;
	if s:sub(1, 1) == "-" then p = false; s = s:sub(2, #s); end
	if not Private.verifyBase(s) then error("Invalid argument #1 data contains invalid base characters!", 2); end
	for i = 1, #s do
		local c = s:sub(i, i);
		n = n + Private.language_inversed[c] * math.pow(Private.language_length, #s - i);
	end
	return p and n or n * -1;
end

if type(module) == "table" then module.exports = Base;
else return Base; end